<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<head>
	<title>Login - Toko Buku</title>
</head>
<body>

<div class="container">
	<div class="card mt-3 mb-3">
		<div class="card-header">Login - Toko Buku</div>
		<div class="card-body">
				<form action="/Auth/login" method="POST">
				@csrf
					<div class="form-group">
					<label class="label" for="username">
					Username
					</label>
					<input class="form-control" type="text" name="username">
					</div>
					<br>

					<div class="form-group">
					<label class="label" for="password">
					Password
					</label>
					<input class="form-control" type="password" name="password">
					</div>
					<input class="btn btn-success btn-md" type="submit" name="login" value="login">
				</form>
		</div>
	</div>
</div>

</body>
</html>