<!DOCTYPE html>
<html>
<head>
	<title>Update User - Toko Buku</title>
</head>
<body>

	@foreach($user as $usr)
	<form action="/admin/user/update" method="POST">
		@csrf
		<input type="hidden" name="id"  value="{{$usr->id}}">
		<label>Username</label>
		<input type="text" name="name" value="{{$usr->name}}">
		<br>
		<label>Email</label>
		<input type="mail" name="email" value="{{$usr->email}}">
		<br>
		<label>Password</label>
		<input type="password" name="password" value="{{$usr->password}}">
		<br>
		<input type="submit" name="update" value="Update">
	</form>
	@endforeach

</body>
</html>