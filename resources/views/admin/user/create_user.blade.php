<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<head>
	<title>Buat User - Toko Buku</title>
</head>
<body>

<div class="container mt-3 mb-3">

	<a class="btn btn-danger" href="/admin/user">
		Back
	</a>

	<div class="card mt-3">
		<div class="card-header">
			<h3 class="title">
				Add User
			</h3>
		</div>
		<div class="card-body">
		<form action="/admin/user/create" method="POST">
		@csrf
		<div class="form-group">
		<label>Username</label>
		<input class="form-control" type="text" name="name">
		<br>
		</div>
		<div class="form-group">
		<label>Email</label>
		<input class="form-control" type="mail" name="email">
		<br>
		</div>
		<div class="form-group">
		<label>Password</label>
		<input class="form-control" type="password" name="password">
		<br>
		</div>
		<input class="btn btn-dark" type="submit" name="add" value="Add User">
	</form>
		</div>
	</div>
</div>

</body>
</html>